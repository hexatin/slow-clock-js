Slow Clock JS
=============

A HTML5/pure-JS clone of the slow watch (a 24-hour analog clock with one hand).

Leave it open in a browser window on another screen, and watch time pass.
