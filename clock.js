const canvas = document.getElementById('canvas');
canvas.width = 640;
canvas.height = 640;
const ctx = canvas.getContext('2d');

var face = new Image();
face.src = "slow-watch-face.svg";

var hand = new Image();
hand.src = "slow-watch-hand.svg";

handAngle = 0;

function updateHandAngle(){
    var d = new Date();
    hrs = d.getHours();
    mins = d.getMinutes();
    secs = d.getSeconds();
    secsSinceMidnight = (hrs*60*60) + (mins*60) + secs;

    // Linearly map secsSinceMidnight onto radians around a circle
    frac = secsSinceMidnight / (24*60*60);
    handAngle = frac * (2*Math.PI);
}

function rotateHand(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    //ctx.restore();
    ctx.drawImage(face, 0, 0);
    ctx.save();
    handX = (canvas.width/2)-(hand.width/2)-4.5;
    handY = (canvas.height/2)-14;
    //ctx.translate(canvas.width/2, canvas.height/2);
    ctx.translate(handX, handY);
    ctx.rotate(handAngle);
    //ctx.drawImage(hand, -hand.width-4, -(hand.height / 2)-13);
    ctx.drawImage(hand, -7, -hand.height/2+1);
    ctx.restore();
    //ctx.translate(-canvas.width/2, -canvas.height/2);
    //ctx.rotate(-handAngle);
    updateHandAngle();
    console.log(handAngle)
    requestAnimationFrame(rotateHand);
}
requestAnimationFrame(rotateHand);
